package com.android.vy;

import android.app.Activity;

import java.util.HashMap;
import java.util.Map;

/**
 * 激励视频Map
 */
public class JiliMap {
    private HashMap<String, JiliBean> map = new HashMap<String, JiliBean>();

    /**
     * 新加一个视频广告
     *
     * @param act
     * @param adName 该广告的标志
     */
    public void add(Activity act, String adName) {
        JiliBean jiliBean = new JiliBean(act);
        map.put(adName, jiliBean);
    }

    public void showAd(String adName) {
        try {
            map.get(adName).getRewardVideo().showAd();
        } catch (Exception e) {
        }
    }

    /**
     * 是否看过了广告
     *
     * @param adName
     * @return
     */
    public boolean isRewardVideoFinish(String adName) {
        return map.get(adName).isRewardVideoFinish();
    }


    public class JiliBean {
        private RewardVideo rewardVideo;
        private boolean isRewardVideoFinish;

        public JiliBean(Activity act) {
            rewardVideo = new RewardVideo(act, true, new RewardVideo.XmbRewardVideoADListener() {
                @Override
                public void onReward(Map<String, Object> map) {
                    isRewardVideoFinish = true;
                }
                public void onNotReady() {
                    isRewardVideoFinish = true;
                }
                public void onPoppingError() {
                    isRewardVideoFinish = true;
                }
            });
        }

        public RewardVideo getRewardVideo() {
            return rewardVideo;
        }

        public void setRewardVideo(RewardVideo rewardVideo) {
            this.rewardVideo = rewardVideo;
        }

        public boolean isRewardVideoFinish() {
            return isRewardVideoFinish;
        }

        public void setRewardVideoFinish(boolean rewardVideoFinish) {
            isRewardVideoFinish = rewardVideoFinish;
        }
    }

}
