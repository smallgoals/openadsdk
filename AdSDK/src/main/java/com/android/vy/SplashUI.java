package com.android.vy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.android.OpenAdSDK.R;
import com.android.vy.csj.CsjSplashAd;
import com.android.vy.gdt.GdtSplashAd;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.BarUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.Fs;
import com.nil.sdk.utils.Gid;
import com.nil.sdk.utils.NetworkUtils;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.nil.vvv.utils.ZFactory;

/**
 * [1240]Gdt开屏广告展示类<br>
 *2021-6-11 上午10:11:59<br>
 */
public class SplashUI extends BaseAppCompatActivity {
    private final static String TAG = SplashUI.class.getSimpleName();
    public static final int START_AS_CP_REQUEST_CODE = 1040;
    private boolean mIsAutoFinish = false;
    private boolean isStartAsCP;
    //是否跳转到主页面
    private boolean canJump;

    AbstractSplashAd mainSplashAd, resSplashAd;

    public static void startAsCP(Activity act, boolean isAutoFinish){
        Intent it = new Intent(act, SplashUI.class);
        it.putExtra("isStartAsCP", true);
        it.putExtra("auto_finish", isAutoFinish);
        ActivityUtils.startActivityForResult(act, it, START_AS_CP_REQUEST_CODE);
    }
    public static void start(Context act, boolean isAutoFinish) {
        try {
            Intent it = new Intent(act, SplashUI.class);
            it.putExtra("auto_finish", isAutoFinish);
            BaseUtils.startActivity(act, it);
        }catch (Exception e){
            LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
            e.printStackTrace();
        }
    }

    //广告预初始化，建议放到同意隐私政策后的WelcomeActivity界面：
    public static void preInit(){
        preInitGdt(Utils.getApp());
    }
    public static void preInitGdt(Context ctx){//兼容老版本SDK：
        if(AdSwitchUtils.Ads.Csj.flag){
            CsjSplashAd.getInstance().preInit(ctx);
        }
        if(AdSwitchUtils.Ads.Qq.flag){
            GdtSplashAd.getInstance().preInit(ctx);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsAutoFinish = getIntent().getBooleanExtra("auto_finish", false);
        isStartAsCP = getIntent().getBooleanExtra("isStartAsCP", false);
        SplashUI.preInitGdt(getActivity()); // 广告预加载

        BarUtils.setStatusBarVisibility(this, false);

        if (mIsAutoFinish) {
            BaseUtils.popDebugToast("回到APP，进入定时闪屏>>");
        }

        setFullScreen();
        setContentView(R.layout.splash_ui);
        hideActionBar();

        View rootView = this.getWindow().getDecorView();
        if(AdSwitchUtils.Ads.Csj.flag){
            mainSplashAd = new CsjSplashAd(this, rootView, new AbstractSplashAd.SplashAdCallback() {
                @Override
                public void next() {
                    gotoNext();
                }
                public void nextReserve(){
                    if(resSplashAd != null && NetworkUtils.isNetworkAvailable(getActivity())) {
                        resSplashAd.loadMyAd();
                    }else{
                        gotoMainUI();
                    }
                }
                public void nextMainUI() {
                    gotoMainUI();
                }
            });

            if(AdSwitchUtils.Ads.Qq.flag){
                resSplashAd = new GdtSplashAd(this, rootView, new AbstractSplashAd.SplashAdCallback() {
                    @Override
                    public void next() {
                        gotoNext();
                    }
                    public void nextMainUI() {
                        gotoMainUI();
                    }
                });
            }
        }else {
            mainSplashAd = new GdtSplashAd(this, rootView, new AbstractSplashAd.SplashAdCallback() {
                @Override
                public void next() {
                    gotoNext();
                }
                public void nextMainUI() {
                    gotoMainUI();
                }
            });
        }

        //step3:拉取并加载开屏广告
        mainSplashAd.loadMyAd();
    }

    /** 开屏页一定要禁止用户对返回按钮的控制，否则将可能导致用户手动退出了App而广告无法正常曝光和计费 */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void gotoMainUI(){
        //errNums = MaxNums;
        if(mIsAutoFinish){
            finish();
        }else if(isStartAsCP){
            //do nothing
        }else{
            String mainActivity = Gid.getS(this, R.string.main_activity);
            if (StringUtils.noNullStr(mainActivity)) {    //main ui
                BaseUtils.gotoActivity(this, mainActivity, 0);
            } else if (Fs.findMyClass(ZFactory.sZz)) {    //ZFactory
                BaseUtils.gotoActivity(this, ZFactory.sZz, 0);
            }
        }
    }

    /**
     * 设置一个变量来控制当前开屏页面是否可以跳转，当开屏广告为普链类广告时，<br>
     * 点击会打开一个广告落地页，此时开发者还不能打开自己的App主页。当从广告落地页返回以后，<br>
     * 才可以跳转到开发者自己的App主页；当开屏广告是App类广告时只会下载App。<br>
     */
    private void gotoNext() {
        if (canJump) {
            gotoMainUI();
        } else {
            canJump = true;
        }
    }
    protected void onPause() {
        super.onPause();
        canJump = false;
    }
    protected void onResume() {
        super.onResume();
        if (canJump) {
            gotoNext();
        }
        canJump = true;
    }
}