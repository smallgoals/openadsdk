package com.android.vy.gdt;

import android.app.Activity;

import com.android.OpenAdSDK.R;
import com.android.vy.AbstractRewardVideo;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.qq.e.ads.rewardvideo.RewardVideoAD;
import com.qq.e.ads.rewardvideo.RewardVideoADListener;
import com.qq.e.comm.util.AdError;

import java.util.Map;

public class GdtRewardVideo extends AbstractRewardVideo{
    private final static String TAG = GdtRewardVideo.class.getSimpleName();
    private RewardVideoAD sRewardVideoAD = null;

    public GdtRewardVideo(Activity act, boolean volumeOn, XmbRewardVideoADListener callback) {
        super(act, volumeOn, callback);
    }

    public void initAd() {
        /** 配置Gdt的开屏ID和设置应用版本号*/
        String qqID = AdSwitchUtils.getInstance(Utils.getApp()).getOnlineValue(AdSwitchUtils.Ads.Qq.getKey(), R.string.qq_id);
        String[] idAry = (qqID == null) ? null : qqID.split("##");
        if (idAry != null && idAry.length >= 6 && idAry[5].length() >= 10) {
            String rewardID = StringUtils.getAdPosID(idAry[5]);
            BaseUtils.addMap("GdtRewardVideo.initAd", rewardID);

            //初始化激励视频广告
            sRewardVideoAD = new RewardVideoAD(Utils.getApp(), rewardID,
                    new GdtRewardVideoADListener(callback) {
                        @Override
                        public void onReward(Map<String, Object> map) {
                            super.onReward(map);
                            initAd();
                        }
                        public void onADClose() {
                            super.onADClose();

                            //用户看完广告或提前放弃激励都会调用此关闭接口
                            initAd();
                        }
                        public void onError(AdError adError) {
                            try {
                                BaseUtils.addMap("GdtRewardVideo.initAd.onError", "err=" + GdtSplashAd.gdtErr(adError) + ",jlspErrNums=" + curErrorNums);
                                if (GdtSplashAd.isRetry(adError) && (curErrorNums < MAX_ERROR_NUMS)) {
                                    curErrorNums++;
                                    LogUtils.e("gdtjlsp.curErrorNums:" + curErrorNums);

                                    //激励广告错误时，进行加载下一个激励
                                    if (sRewardVideoAD != null) sRewardVideoAD.loadAD();
                                } else {
                                    if(res != null){
                                        sRewardVideoAD = null;
                                        res.initAd();
                                    }else {
                                        //其它错误，直接抛出回调
                                        if (callback != null) callback.onError(adError);
                                    }
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                        public void onADLoad() {
                            super.onADLoad();

                            if (BaseUtils.getIsDebug(Utils.getApp()) || AdSwitchUtils.Sws.O2.flag){
                                //激励视频：添加应用下载类广告的确认弹窗对话框
                                GdtSplashAd.addDownloadConfirmListener(sRewardVideoAD);
                            }
                        }
                    }, volumeOn);
            sRewardVideoAD.loadAD();
        } else {
            // ad is null
            LogUtils.eTag(TAG, "rewardID is error");
        }
    }

    @Override
    public void showAd() {
        if (sRewardVideoAD != null && sRewardVideoAD.isValid()) {
            if (act == null) {
                sRewardVideoAD.showAD();
            } else {
                sRewardVideoAD.showAD(act);
            }
            if (callback != null) callback.isPopping = true;
        } else {
            if(res != null){
                res.showAd();
            }else {
                if (callback != null) callback.onNotReady();
            }
        }
    }

    public abstract class GdtRewardVideoADListener implements RewardVideoADListener {
        XmbRewardVideoADListener mCallback;

        public GdtRewardVideoADListener(XmbRewardVideoADListener callback) {
            this.mCallback = callback;
        }

        @Override
        public void onADLoad() {
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "RewardVideoAD-onADLoad");
            /*String msg = "load ad success ! expireTime = " + new Date(System.currentTimeMillis
            () + sRewardVideoAD.getExpireTimestamp() - SystemClock.elapsedRealtime());
            msg += ", eCPM = " + sRewardVideoAD.getECPM() + " , eCPMLevel = " + sRewardVideoAD
            .getECPMLevel();
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "RewardVideoAD-onADLoad"+msg);
            NbToast.showToast(msg);*/
            if (mCallback != null) mCallback.onADLoad();

            //添加应用下载类广告的确认弹窗对话框
            GdtSplashAd.addDownloadConfirmListener(sRewardVideoAD);
        }

        @Override
        public void onVideoCached() {
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "RewardVideoAD-onVideoCached");
            if (mCallback != null) mCallback.onVideoCached();
        }

        @Override
        public void onADShow() {
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "RewardVideoAD-onADShow");
            if (mCallback != null) mCallback.onADShow();
        }

        @Override
        public void onADExpose() {
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "RewardVideoAD-onADExpose");
            if (mCallback != null) mCallback.onADExpose();
        }

        @Override
        public void onReward(Map<String, Object> map) {
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "RewardVideoAD-onReward");
            if (mCallback != null) mCallback.onReward(map);
        }

        @Override
        public void onADClick() {
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "RewardVideoAD-onADClick");
            if (mCallback != null) mCallback.onADLoad();
        }

        @Override
        public void onVideoComplete() {
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "RewardVideoAD-onVideoComplete");
            if (mCallback != null) mCallback.onVideoComplete();
        }

        @Override
        public void onADClose() {
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "RewardVideoAD-onADClose");
            if (mCallback != null) mCallback.onADClose();
        }

        @Override
        public void onError(AdError adError) {
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "RewardVideoAD-onError-->" + GdtSplashAd.gdtErr(adError));
            if (mCallback != null) mCallback.onError(adError);
        }

    }
}
