package com.android.vy.csj;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.android.OpenAdSDK.R;
import com.android.vy.AbstractSplashAd;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.Utils;
import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.CSJAdError;
import com.bytedance.sdk.openadsdk.CSJSplashAd;
import com.bytedance.sdk.openadsdk.CSJSplashCloseType;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdLoadType;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTAppDownloadListener;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.PropertiesUtil;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;

/**
 * 开屏广告Activity示例
 */
public class CsjSplashAd extends AbstractSplashAd {
    private static final String TAG = CsjSplashAd.class.getSimpleName();

    private static CsjSplashAd sCsjSplashAd;
    public static CsjSplashAd getInstance(){
        if(sCsjSplashAd == null){
            sCsjSplashAd = new CsjSplashAd();
        }
        return sCsjSplashAd;
    }

    private CsjSplashAd(){}
    public CsjSplashAd(Activity activity, View view, SplashAdCallback splashAdCallback) {
        super(activity, view, splashAdCallback);
    }

    public void preInit(Context ctx){
        try {
            if (ctx == null) ctx = Utils.getApp();
            //穿山甲SDK初始化
            //强烈建议在应用对应的Application#onCreate()方法中调用，避免出现content为null的异常
            TTAdManagerHolder.init(ctx.getApplicationContext());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void loadMyAd(){
        /** 配置Gdt的开屏ID和设置应用版本号*/
        String appid = AdSwitchUtils.getInstance(Utils.getApp()).getOnlineValue(AdSwitchUtils.Ads.Csj.getKey(), R.string.csj_id);
        String[] idAry = (appid == null) ? null : appid.split("##");
        if(idAry != null && idAry.length >= 5){
            String kpID = StringUtils.getAdPosID(idAry[4]);
            BaseUtils.addMap("CsjSplashAd.loadMyAd", kpID);

            if(!AppUtils.hasAgreePrivacyPolicy()) {
                gotoMainUI();
            }else if(kpID.length() < 9) {
                gotoNextReserve();
            }else{
                //加载开屏广告
                loadSplashAd(kpID);
                BaseUtils.addMap("csjkp.errNums", errNums);
            }
        }else{
            gotoNextReserve();
        }
    }

    /**
     * 加载开屏广告
     */
    private void loadSplashAd(String kpID) {
        //step2:创建TTAdNative对象
        TTAdNative mTTAdNative = TTAdManagerHolder.get().createAdNative(activity);

        //step3:创建开屏广告请求参数AdSlot,具体参数含义参考文档
        AdSlot adSlot = new AdSlot.Builder()
                .setCodeId(kpID)
                .setImageAcceptedSize(ScreenUtils.getScreenWidth(), ScreenUtils.getScreenHeight())
                .setAdLoadType(TTAdLoadType.PRELOAD)    //PRELOAD预加载、LOAD实时加载
                .build();

        splashContainer = view.findViewById(R.id.splash_container);

        //step4:请求广告，调用开屏广告异步请求接口，对请求回调的广告作渲染处理
        mTTAdNative.loadSplashAd(adSlot, new TTAdNative.CSJSplashAdListener() {
            @Override
            public void onSplashLoadSuccess(CSJSplashAd csjSplashAd) {
                if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "开屏广告加载成功");
            }

            public void onSplashLoadFail(CSJAdError csjAdError) {
                if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "开屏广告加载失败");
                onMyHandleFail(csjAdError);
            }
            public void onSplashRenderSuccess(CSJSplashAd ad) {
                if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "开屏广告渲染请求成功");
                if (ad == null) {
                    gotoNextReserve(); //如果加载当前开屏失败，尝试加载其它开屏
                    return;
                }

                //获取SplashView
                View view = ad.getSplashView();
                if (view != null && splashContainer != null && !activity.isFinishing()) {
                    splashContainer.setVisibility(View.VISIBLE);
                    splashContainer.removeAllViews();
                    //把SplashView 添加到ViewGroup中,注意开屏广告view：width >=70%屏幕宽；height >=50%屏幕高
                    splashContainer.addView(view);
                } else {
                    gotoNextReserve(); //如果加载当前开屏失败，尝试加载其它开屏
                    return;
                }

                //设置SplashView的交互监听器
                ad.setSplashAdListener(new CSJSplashAd.SplashAdListener() {
                    @Override
                    public void onSplashAdShow(CSJSplashAd csjSplashAd) {
                        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "开屏广告展示");
                    }
                    public void onSplashAdClick(CSJSplashAd csjSplashAd) {
                        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "开屏广告点击");
                    }
                    //开屏点击跳过以及倒计时结束统一收敛到该接口
                    public void onSplashAdClose(CSJSplashAd csjSplashAd, int closeType) {
                        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "开屏广告关闭");
                        if (closeType == CSJSplashCloseType.CLICK_SKIP) {
                            //开屏广告点击跳过
                            gotoMainUI();
                        } else if (closeType == CSJSplashCloseType.COUNT_DOWN_OVER) {
                            //开屏广告倒计时结束
                            gotoNext();
                        } else if (closeType == CSJSplashCloseType.CLICK_JUMP) {
                            //开屏广告点击跳转
                            gotoNext();
                        } else {
                            gotoMainUI();
                        }
                    }
                });

                if (ad.getInteractionType() == TTAdConstant.INTERACTION_TYPE_DOWNLOAD) {
                    ad.setDownloadListener(new TTAppDownloadListener() {
                        boolean hasShow = false;

                        @Override
                        public void onIdle() {}
                        public void onDownloadActive(long totalBytes, long currBytes, String fileName, String appName) {
                            if (!hasShow) {
                                if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "下载中...");
                                hasShow = true;
                            }
                        }
                        public void onDownloadPaused(long totalBytes, long currBytes, String fileName, String appName) {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "下载暂停...");
                        }
                        public void onDownloadFailed(long totalBytes, long currBytes, String fileName, String appName) {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "下载失败...");
                        }
                        public void onDownloadFinished(long totalBytes, String fileName, String appName) {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "下载完成...");
                        }
                        public void onInstalled(String fileName, String appName) {
                            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "安装完成...");
                        }
                    });
                }
            }
            public void onSplashRenderFail(CSJSplashAd csjSplashAd, CSJAdError csjAdError) {
                if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "开屏广告渲染请求失败");
                onMyHandleFail(csjAdError);
            }
            public void onMyHandleFail(CSJAdError csjAdError){
                if(csjAdError == null) {
                    gotoNextReserve(); //如果加载当前开屏失败，尝试加载其它开屏
                    return;
                }
                int code = csjAdError.getCode();
                String message = csjAdError.getMsg();
                LogUtils.eTag(TAG, String.valueOf(message));
                BaseUtils.addMap("CsjSplashAd.loadSplashAd.onMyHandleFail", "err="+ csjErr(code)+",id="+kpID);

                //如果媒体想判断是否是超时回调可以通过  csjAdError.getCode() == 23进行判断
                if(code == 23){
                    LogUtils.wTag(TAG, "开屏广告加载超时");
                    gotoNextReserve(); //如果加载当前开屏失败，尝试加载其它开屏
                }else if(!isRetry(code) || errNums++ >= MaxNums){
                    gotoNextReserve(); //如果加载当前开屏失败，尝试加载其它开屏
                }else{
                    loadMyAd();
                }
            }
        }, AD_TIME_OUT);
    }

    public static String csjErr(int code){
        String msg = "err is null";
        if(code > -1000) {
            msg = code + "::" + PropertiesUtil.getInstance().getValue(R.raw.csj_error_code, code + "");
        }
        return msg;
    }
    public static boolean isRetry(int code){
        return isRetryCode(code);
    }
    private static boolean isRetryCode(int code){
        boolean isOK = false;
        //错误码 https://developers.adnet.qq.com/doc/android/union/union_debug
        int[] codes = {
                20001,   //没有合适的广告返回而导致的请求没有填充,偶现属于正常情况
                -5, -6, -7,//BannerAd、插屏、开屏加载图片失败
                -10, -11, -12,//缓存解析失败、缓存过期、缓存中没有开屏广告
                123456789 //结尾标识
        };
        for(int c : codes){
            if(c == code){
                isOK = true;
                break;
            }
        }
        return isOK;
    }
}
