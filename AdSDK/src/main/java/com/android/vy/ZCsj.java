package com.android.vy;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;

import com.android.vy.csj.CsjSplashAd;
import com.android.vy.csj.TTAdManagerHolder;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdDislike;
import com.bytedance.sdk.openadsdk.TTAdLoadType;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTFullScreenVideoAd;
import com.bytedance.sdk.openadsdk.TTNativeExpressAd;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.ZzAdUtils.AbVVV;

import java.util.List;

/**
 * 穿山甲广告；<br>
 * 2022-11-22 上午10:11:59<br>
 */
public class ZCsj extends AbVVV {
	private static final String TAG = ZCsj.class.getSimpleName();
	public int hfErrNums = 0;
	public int cpErrNums = 0;
	public int MaxNums = 3;

	TTNativeExpressAd bAdV2;
	public void addBarBn(final Activity act, final ViewGroup vg, final String appid) throws Exception {
		BaseUtils.addMap("ZCsj.addBarBn","initAd");
		if(!AppUtils.hasGdtPermission(act) && res != null){
			res.addBannerAd(act);
			return;
		}

		SplashUI.preInit();
		/*if(bAdV2 != null){
			bAdV2.destroy();
			bAdV2 = null;
		}*/

		//step2:创建TTAdNative对象，createAdNative(Context context) banner广告context需要传入Activity对象
		TTAdNative ttAdNative = TTAdManagerHolder.get().createAdNative(act);

		//step3:(可选，强烈建议在合适的时机调用):申请部分权限，如read_phone_state,防止获取不了imei时候，下载类广告没有填充的问题。
		//TTAdManagerHolder.get().requestPermissionIfNecessary(act);

		//step4:创建广告请求参数AdSlot,具体参数含义参考文档
		final String[] idAry = appid.split("##");
		int w = SizeUtils.px2dp(ScreenUtils.getScreenWidth()); //获取手机屏幕宽，然后转化为DP
		AdSlot adSlot = new AdSlot.Builder()
				.setCodeId(StringUtils.getAdPosID(idAry[1])) //广告位id
				.setAdCount(1) //请求广告数量为1到3条
				//.setExpressViewAcceptedSize(ScreenUtils.getScreenWidth(), 60) //单位dp
				//.setExpressViewAcceptedSize(320, 50) //单位dp，横幅为320x50尺寸
				.setExpressViewAcceptedSize(w, w * 50/320f) //单位dp
				.build();

		//step5:请求广告，对请求回调的广告作渲染处理
		ttAdNative.loadBannerExpressAd(adSlot, new TTAdNative.NativeExpressAdListener() {
			@Override
			public void onError(int code, String message) {
				BaseUtils.addMap("ZCsj.loadBannerExpressAd.onError", "err="+ CsjSplashAd.csjErr(code));
				// 广告是可以重试的，且错误次数小于3；则再次拉取并显示广告。
				if (CsjSplashAd.isRetry(code) && hfErrNums < MaxNums) {
					try {
						hfErrNums++;
						addBarBn(act, vg, appid);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else{
					if (vg != null) vg.removeAllViews();
					if (res != null) res.addBannerAd(act);
				}
			}
			public void onNativeExpressAdLoad(List<TTNativeExpressAd> ads) {
				if (ads == null || ads.size() == 0) {
					if (vg != null) vg.removeAllViews();
					if (res != null) res.addBannerAd(act);
					return;
				}
				bAdV2 = ads.get(0);
				bAdV2.setSlideIntervalTime(30 * 1000);//轮播时间30s
				bAdV2.setExpressInteractionListener(new TTNativeExpressAd.ExpressAdInteractionListener() {
					@Override
					public void onAdClicked(View view, int type) {
						if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "广告被点击");
					}
					public void onAdShow(View view, int type) {
						if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "广告展示");
					}
					public void onRenderFail(View view, String msg, int code) {
						LogUtils.eTag(TAG, msg + " code:" + code);
					}
					public void onRenderSuccess(View view, float width, float height) {
						//返回view的宽高 单位 dp
						if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "渲染成功");
						if (vg != null) {
							vg.removeAllViews();
							vg.addView(view);
						}
					}
				});
				//响应关闭按钮：
				bAdV2.setDislikeCallback(act, new TTAdDislike.DislikeInteractionCallback() {
					@Override
					public void onShow(){}
					public void onCancel(){}
					public void onSelected(int position, String value, boolean enforce) {
						if (vg != null) vg.removeAllViews();
					}
				});

				//step6:显示广告
				if(bAdV2 != null) bAdV2.render();
			}
		});
	}

	TTFullScreenVideoAd iAdV2;
	public void showCPAd(final Activity act, final String appid){
		BaseUtils.addMap("ZCsj.showNewCPAd","initAd");
		if(AppUtils.isClosePushAd()){
			if(iAdV2 != null) iAdV2 = null;
			return;
		}

		if(!AppUtils.hasGdtPermission(act) && res != null){
			res.openCpAd(act);
			return;
		}

		SplashUI.preInit();
		if(iAdV2 != null){
			iAdV2 = null;
		}

		//step2:创建TTAdNative对象,用于调用广告请求接口
		TTAdNative ttAdNative = TTAdManagerHolder.get().createAdNative(act);
		//step3:可选，申请部分权限，如read_phone_state,防止获取不了imei时候，下载类广告没有填充的问题。
		//TTAdManagerHolder.get().requestPermissionIfNecessary(act);

		//step4:创建广告请求参数AdSlot,注意其中的setNativeAdtype方法，具体参数含义参考文档
		String[] idAry = appid.split("##");
		AdSlot adSlot = new AdSlot.Builder()
				.setCodeId(StringUtils.getAdPosID(idAry[2])) //广告位id
				.setAdCount(1) //请求广告数量为1到3条
				.setSupportDeepLink(true)
				.setAdLoadType(TTAdLoadType.LOAD)//推荐使用，用于标注此次的广告请求用途为预加载（当做缓存）还是实时加载，方便后续为开发者优化相关策略
				.build();

		//step5:请求广告，对请求回调的广告作渲染处理
		ttAdNative.loadFullScreenVideoAd(adSlot, new TTAdNative.FullScreenVideoAdListener() {
			//请求广告失败
			@Override
			public void onError(int code, String message) {
				BaseUtils.addMap("ZCsj.showNewCPAd.onError", "err=" + CsjSplashAd.csjErr(code));
				// 广告是可以重试的，且错误次数小于3；则再次拉取并显示广告。
				if (CsjSplashAd.isRetry(code) && cpErrNums < MaxNums) {
					try {
						cpErrNums++;
						showCPAd(act, appid);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else{
					if (res != null) res.openCpAd(act);
				}
			}
			//广告物料加载完成的回调
			public void onFullScreenVideoAdLoad(TTFullScreenVideoAd ad) {
				iAdV2 = ad;
			}
			//广告视频/图片加载完成的回调，接入方可以在这个回调后展示广告
			public void onFullScreenVideoCached() {
				if (iAdV2 != null) {
					//广告交互监听器
					iAdV2.setFullScreenVideoAdInteractionListener(new TTFullScreenVideoAd.FullScreenVideoAdInteractionListener() {
						@Override
						public void onAdShow() {
							if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "广告的展示回调");
						}
						public void onAdVideoBarClick() {
							if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "广告的下载bar点击回调");
						}
						public void onAdClose() {
							if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "广告关闭的回调");
						}
						public void onVideoComplete() {
							if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "视频播放完毕的回调");
						}
						public void onSkippedVideo() {
							if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "跳过视频播放");
						}
					});

					//展示广告，并传入广告展示的场景
					iAdV2.showFullScreenVideoAd(act, TTAdConstant.RitScenes.GAME_GIFT_BONUS, null);
					iAdV2 = null;
				}
			}
			public void onFullScreenVideoCached(TTFullScreenVideoAd ttFullScreenVideoAd) {
				iAdV2 = ttFullScreenVideoAd;
				if (iAdV2 != null) {
					//广告交互监听器
					iAdV2.setFullScreenVideoAdInteractionListener(new TTFullScreenVideoAd.FullScreenVideoAdInteractionListener() {
						@Override
						public void onAdShow() {
							if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "广告的展示回调");
						}
						public void onAdVideoBarClick() {
							if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "广告的下载bar点击回调");
						}
						public void onAdClose() {
							if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "广告关闭的回调");
						}
						public void onVideoComplete() {
							if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "视频播放完毕的回调");
						}
						public void onSkippedVideo() {
							if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "跳过视频播放");
						}
					});

					//展示广告，并传入广告展示的场景
					iAdV2.showFullScreenVideoAd(act, TTAdConstant.RitScenes.GAME_GIFT_BONUS, null);
					iAdV2 = null;
				}
			}
		});

	}
}
